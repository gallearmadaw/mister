@extends('layout.index')

@section('content')
<div class="container-fluid">
<div class="card card-register mx-auto mt-5">
      <div class="card-header">Register an Account</div>
      <div class="card-body">
        @foreach($user as $u)
        <form method="post" action="{{ route('updateuser') }}" >
          @csrf
          <input type="hidden" name="id" value="{{ $u->id }}">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input value="{{ $u->name }}" name="name" type="text" id="firstName" class="form-control" placeholder="Usename" required="required" autofocus="autofocus">
                  <label for="firstName">Username</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input value="{{ $u->email  }}" name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required">
              <label for="inputEmail">Email address</label>
            </div>
          </div>
          </div>
          <button type="submit" class="btn btn-primary ">Update</button>
          <a href="/user" class="btn btn-info ">Kembali</a>
          @endforeach
        </form>
      </div>
    </div>
</div>
@endsection