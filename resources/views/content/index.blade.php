@extends('layout.index')

@section('content')
<div class="card mb-3">
    <div class="card-header">
    <i class="fas fa-table"></i>
    Users</div>
    <div class="card-body">
    
    <div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($user as $usr)
                        <tr>
                            <td>{{ $usr->name }}</td>
                            <td>{{ $usr->email }}</td>
                            <td>
                                <a href="{{action('UserController@edit',$usr->id)}}" class="btn btn-success">Read</a>
                                <a href="/deleteuser/{{$usr->id}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
</table>
    </div>
    </div>
</div>
@endsection