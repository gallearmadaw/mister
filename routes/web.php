<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('login');
});

Route::resource('user','UserController');
Route::post('updateuser','UserController@update')->name('updateuser');
Route::get('deleteuser/{user}','UserController@destroy');
Route::post('userlogin','UserController@validasi')->name('userlogin');
Route::get('logout','UserController@logout')->name('userlogout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
